from net.morgz.core.fizzbuzz import FizzBuzz

__author__ = 'jmorgan'

import unittest


class FizzBuzzTest(unittest.TestCase):
    def setUp(self):
        self.fizzbuzz = FizzBuzz()

    def test_can_create_fizzbuzz_instance(self):
        self.assertIsInstance(self.fizzbuzz, FizzBuzz)

    def test_if_divisible_by_three_then_say_fizz(self):
        numbers = [3]
        results = []

        for number in numbers:
            results.append(self.fizzbuzz.say(number))

        for result in results:
            self.assertEqual('fizz', result)

    def test_if_divisble_by_five_then_say_buzz(self):
        numbers = [5]
        results = []

        for number in numbers:
            results.append(self.fizzbuzz.say(number))

        for result in results:
            self.assertEqual('buzz', result)

    def test_if_divisible_by_three_or_five_then_say_fizzbuzz(self):
        numbers = [15]
        results = []

        for number in numbers:
            results.append(self.fizzbuzz.say(number))

        for result in results:
            self.assertEqual('fizzbuzz', result)

    def test_if_not_divisble_by_three_or_five_say_number(self):
        numbers = [7]
        results = []

        for number in numbers:
            results.append(self.fizzbuzz.say(number))

        for result in results:
            self.assertEqual('7', result)

    def test_sequence_results_correct_results(self):
        numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        results = []

        for number in numbers:
            results.append(self.fizzbuzz.say(number))

        self.assertEqual('1',        results[0])
        self.assertEqual('2',        results[1])
        self.assertEqual('fizz',     results[2])
        self.assertEqual('4',        results[3])
        self.assertEqual('buzz',     results[4])
        self.assertEqual('fizz',     results[5])
        self.assertEqual('7',        results[6])
        self.assertEqual('8',        results[7])
        self.assertEqual('fizz',     results[8])
        self.assertEqual('buzz',     results[9])
        self.assertEqual('11',       results[10])
        self.assertEqual('fizz',     results[11])
        self.assertEqual('13',       results[12])
        self.assertEqual('14',       results[13])
        self.assertEqual('fizzbuzz', results[14])

if __name__ == '__main__':
    unittest.main()


